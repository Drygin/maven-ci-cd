package com.devops;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import javax.ws.rs.core.Application;
import static org.junit.Assert.assertEquals;

public class CalculusResourceTest extends JerseyTest {

    @Test
    public void testSayWelcome() {

        final String hello = target("welcome").request().get(String.class);

        assertEquals("Welcome to the best Calculus App, ever!", hello);
    }

    @Override
    protected Application configure() {

        return new ResourceConfig(CalculusResource.class);
    }
}
