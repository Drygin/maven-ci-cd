package com.devops;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public final class CalculusResource {

    @GET
    @Path("/welcome")
    @Produces(MediaType.TEXT_PLAIN)
    public String sayWelcome() {

        return "Welcome to the best Calculus App, ever!";
    }
}
